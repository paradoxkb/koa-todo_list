/**
 * Created by watcher on 7/16/18.
 */
const router = require('koa-router')();
const koaBody = require('koa-body');
const { Task, SubTask } = require('../db/db');

const STATUS_PROGRESS = 'progress';
const STATUS_WAITING = 'waiting';
const STATUS_DONE = 'done';

router.use(koaBody());

router.get('/tasks', async (ctx, next) => {
	const queryParams = ctx.query;
	const filter = {};

	if (Object.keys(queryParams).length) {
		filter.$or = Object.keys(queryParams).map(key => {
			return {[key]: { $iLike: `%${queryParams[key]}%` }};
		});
		filter.$or.push({
			'$subTasks.text$': { $iLike: `%${Object.values(queryParams)[0]}%` },
		});
	}

	const tasks = await Task.findAll({
		where: filter,
		include: [
			{
				model: SubTask,
				as: 'subTasks',
			},
		],
		order: ['dayNum'],
	});

	ctx.body = tasks;
});

router.post('/tasks', async (ctx, next) => {
	const data = ctx.request.body;
	const created = await Task.create(data);

	ctx.body = created;
});

router.patch('/tasks/:id', async function(ctx) {
	const taskId = ctx.params.id;
	const data = ctx.request.body;
	const filter = {
		where: {
			id: taskId,
		},
	};
	const updated = await Task
		.findOne(filter)
		.then(instance => {
			return instance.updateAttributes(data);
		})
		.catch(err => {
			throw err;
		});

	ctx.body = updated;
});

router.post('/subtasks', async (ctx, next) => {
	const data = ctx.request.body;
	const created = await SubTask.create(data);

	if (data.status !== STATUS_DONE) {
		await Task.findOne({
			where: {
				id: data.taskId,
			},
		}).then(task => {
			return task.updateAttributes({
				status: STATUS_PROGRESS
			});
		});
	}

	ctx.body = created;
});

router.patch('/subtasks/:id', async function(ctx) {
	const subTaskId = ctx.params.id;
	const data = ctx.request.body;
	const filter = {
		where: {
			id: subTaskId,
		},
	};
	const updated = await SubTask
		.findOne(filter)
		.then(instance => {
			return instance.updateAttributes(data);
		})
		.catch(err => {
			throw err;
		});

	// check if parent task available to done
	const parentTask = await Task.findOne({
		where: {
			id: data.taskId,
		},
	});
	const inWaiting = await SubTask.findAll({
		where: {
			taskId: data.taskId,
			status: STATUS_WAITING,
		},
	});
	const updateParent = { status: inWaiting.length ? STATUS_PROGRESS : STATUS_DONE };
	await parentTask.updateAttributes(updateParent);

	ctx.body = updated;
});

router.del('/tasks/:id', async (ctx, next) => {
	const targetId = ctx.params.id;
	const removed = await Task.destroy({ where: { id: ctx.params.id }}, { include: 'subTasks' });

	ctx.body = removed;
});

module.exports = router;
