/**
 * Created by watcher on 8/2/18.
 */
const Sequelize = require('sequelize');

const creds = require('../config/creds.json');
const TaskSchema = require('../models/Task');
const SubTaskSchema = require('../models/SubTask');

const connectionSettings = process.env.NODE_ENV === 'test' ?
	{ dialect: 'sqlite' } :
	{
		host: creds.postgres_url,
		dialect: 'postgres',
		pool: {
			max: 5,
			min: 0,
			acquire: 30000,
			idle: 10000,
		},
	};

const db = new Sequelize(
	'local_test',
	creds.postgres_user,
	creds.postgres_pswd,
	connectionSettings,
);

if (process.env.NODE_ENV !== 'test')
	// db.sync({ force: true });
	db.sync();

const SubTask = new SubTaskSchema({ db });
const Task = new TaskSchema({ db, SubTask: SubTask.model });

module.exports = {
	db,
	Task: Task.model,
	SubTask: SubTask.model
};
