const Sequelize = require('sequelize');
const { STRING, DATE, INTEGER } = Sequelize;

const subTaskSchema = {
	id: {
		type: INTEGER,
		allowNull: false,
		unique: true,
		primaryKey: true,
		autoIncrement: true
	},
	text: {
		type: STRING,
		allowNull: false
	},
	status: {
		type: STRING,
		defaultValue: 'waiting',
		values: ['waiting', 'done']
	},
	taskId: {
		type: INTEGER,
		allowNull: false
	},
}

class SubTask {
	constructor(props) {
		const { db } = props

		if (!db) throw Error('db required')

		this.model = db.define('subtasks', subTaskSchema);
	}
}

module.exports = SubTask;
