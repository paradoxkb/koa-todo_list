import React from 'react';
import $ from 'jquery';

class Helper {
	constructor() {
		this.days = {
			0: 'Mon.',
			1: 'Tue.',
			2: 'Wed.',
			3: 'Thu.',
			4: 'Fri.',
			5: 'Sat.',
			6: 'Sun.',
		};
	}

	async getItems(url = '', condition = {}) {
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Accept', 'application/json');
		const items = await fetch(url, {
			method: 'GET',
			headers,
		})
			.then(res => (res.status === 200 ? res.json() : res))
			.catch(err => {
				console.log(err);
			});

		return items;
	}

	async patchItem(url = '', data) {
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Accept', 'application/json');

		const item = await fetch(url, {
			method: 'PATCH',
			headers,
			body: JSON.stringify(data),
		}).catch(err => { throw err; });

		return item;
	}

	async createItem(url = '', data) {
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		const item = await fetch(url, {
			method: 'POST',
			headers,
			body: JSON.stringify(data),
		}).then(res => res.json());

		return item;
	}

	async deleteItem(url = '') {
		const res = await fetch(url, {
			method: 'DELETE',
		});

		return res;
	}

	createSelectItems (option, items) {
		let itemsToSelect = [],
			valueKey = '',
			labelKey = '';
		switch (option) {
			case 'tasks':
				valueKey = 'id';
				labelKey = 'title';
				break;
			default:
				return itemsToSelect;
		}

		itemsToSelect = items.map((item, i) => (
			<option key={i} value={item[valueKey]}>{item[labelKey]}</option>
		))
		return itemsToSelect;
	}

	toggleLoader(val) {
		const body = $('body');
		if (val) {
			body.addClass('loader-open');
		} else {
			body.removeClass('loader-open');
		}
	}

	serialize(obj) {
		return '?'+Object.keys(obj).reduce(function(a,k){a.push(k+'='+encodeURIComponent(obj[k]));return a},[]).join('&');
	}

}

export default Helper;
