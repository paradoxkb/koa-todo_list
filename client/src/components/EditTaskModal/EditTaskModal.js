import React from 'react';
import { Row, Col, Input, Label, Button, Modal, ModalBody, ModalFooter } from 'reactstrap';

import Helper from '../../utils';

const helper = new Helper();

class EditTaskModal extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			task: props.task || {
				title: '',
				description: '',
				status: 'new',
				dayNum: 0,
			},
		};

		this.changeHandle = this.changeHandle.bind(this);
	}

	changeHandle(event) {
		const { task } = this.state;
		const targetName = event.target.name;

		task[targetName] = event.target.value;

		this.setState({ task });
	}

	render() {
		const { task } = this.state;
		const { toggle, saveTask, isOpen } = this.props;

		return (
			<Modal isOpen={isOpen} toggle={this.toggleEditModal} className="edit-modal">
				<ModalBody>
					<Row className="edit-task-row">
						<Col md={3}>
							<Label for="e_day">Day</Label>
							<Input
								type="select"
								name="dayNum"
								id="e_day"
								defaultValue={task.dayNum}
								onChange={this.changeHandle}
							>
								{Object.keys(helper.days).map((d, i) => (<option key={i} value={d}>{helper.days[d]}</option>))}
							</Input>
						</Col>
						<Col md={3}>
							<Label for="e_title">Title</Label>
							<Input
								type="text"
								name="title"
								id="e_title"
								defaultValue={task.title}
								onChange={this.changeHandle}
							/>
						</Col>
						<Col md={3}>
							<Label for="e_description">Description</Label>
							<Input
								type="text"
								name="description"
								id="e_description"
								defaultValue={task.description}
								onChange={this.changeHandle}
							/>
						</Col>
						<Col md={3}>
							<Label for="e_status">Status</Label>
							<Input
								type="select"
								name="status"
								id="e_status"
								defaultValue={task.status}
								onChange={this.changeHandle}
							>
								<option value="new">New</option>
								<option value="progress">Progress</option>
								<option value="done">Done</option>
							</Input>
						</Col>
					</Row>
				</ModalBody>
				<ModalFooter>
					<Button color="secondary" onClick={toggle}>Cancel</Button>
					<Button color="success" onClick={() => (saveTask(task))}>Save</Button>
				</ModalFooter>
			</Modal>
		);
	}
}

export default EditTaskModal;
