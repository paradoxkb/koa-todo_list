import React from 'react';
import PropTypes from 'prop-types';

import SubTask from '../SubTask/SubTask';
import Helper from '../../utils';

const helper = new Helper();

class Task extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			task: props.task,
		};

		this.addSubTask = this.addSubTask.bind(this);
		this.subTaskSave = this.subTaskSave.bind(this);
	}

	async subTaskSave(subTask) {
		const { task } = this.state;
		const prepared = { ...subTask, taskId: task.id };

		if (subTask.id) {
			await helper.patchItem(`/subtasks/${subTask.id}`, prepared);
		} else {
			await helper.createItem('/subtasks', prepared);
		}
	}

	addSubTask() {
		const { task } = this.state;

		task.subTasks.push({});

		this.setState({ task });
	}

	render() {
		const { task, toggleEditModal, removeTask } = this.props;
		const className = task.status === 'done' ? 'table-success' : 'table-warning';

		return (
			<React.Fragment>
				<tr className={className}>
					<td>{helper.days[task.dayNum]}</td>
					<td>{task.title}</td>
					<td>{task.description}</td>
					<td>{new Date(task.updatedAt).toDateString()}</td>
					<td>{task.status}</td>
					<td>
						<input type='button' className='btn btn-primary' value='Edit' onClick={() => (toggleEditModal(task))}/>
						<input type='button' className='ml-2 btn btn-danger' value='Remove' onClick={() => (removeTask(task.id))}/>
					</td>
				</tr>
				<tr>
					<td colSpan='6' className='text-center'>
						<div className='d-flex justify-content-around'>
							<strong>Subtasks</strong>
							<input type='button' value='Add Subtask' onClick={this.addSubTask}/>
						</div>
					</td>
				</tr>
				{task.subTasks.map((subTask, i) => (
					<SubTask key={i} subTask={subTask} save={this.subTaskSave}/>
				))}
			</React.Fragment>
		);
	}
}

Task.propTypes = {
	task: PropTypes.object.isRequired,
};

export default Task;
