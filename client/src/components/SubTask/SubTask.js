import React from 'react';
import { Input } from 'reactstrap';

const initial = {
	text: '',
	status: 'waiting',
};

class SubTask extends React.Component {
	constructor(props) {
		super(props);

		const subTask = (props.subTask && props.subTask.id) ? props.subTask : initial;

		this.state = {
			subTask,
			editAble: !subTask.id,
		};

		this.changeHandle = this.changeHandle.bind(this);
		this.saveHandle = this.saveHandle.bind(this);
	}

	changeHandle(event) {
		const { subTask } = this.state;
		const targetName = event.target.name;

		subTask[targetName] = event.target.value;

		this.setState({ subTask });
	}

	saveHandle() {
		this.props.save(this.state.subTask);

		this.setState({ editAble: false });
	}

	render() {
		const { subTask, editAble } = this.state;
		const { save } = this.props;
		const className = subTask.status === 'done' ? 'table-success' : 'table-warning';

		const current = editAble ? (
			<tr>
				<td></td>
				<td></td>
				<td><Input type="text" name="text" defaultValue={subTask.text} onChange={this.changeHandle}/></td>
				<td></td>
				<td>
					<Input
						type="select"
						name="status"
						defaultValue={subTask.status}
						onChange={this.changeHandle}
					>
						<option value="waiting">Waiting</option>
						<option value="done">Done</option>
					</Input>
				</td>
				<td><input type="button" className="btn btn-success" value="Save" onClick={this.saveHandle}/></td>
			</tr>
		) : (
			<tr className={className}>
				<td></td>
				<td></td>
				<td>{subTask.text}</td>
				<td></td>
				<td>{subTask.status}</td>
				<td>
					<input
						type="button"
						className="btn btn-primary"
						value="Edit"
						onClick={() => {this.setState({ editAble: true });}}
					/>
				</td>
			</tr>
		);

		return current;
	}
}

export default SubTask;
