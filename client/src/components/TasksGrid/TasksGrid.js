import React from 'react';
import { Table, Input } from 'reactstrap';

import Task from '../Task/Task';
import EditTaskModal from '../EditTaskModal/EditTaskModal';
import Helper from '../../utils';
import './style.scss';

const helper = new Helper();

class TasksGrid extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			tasks: [],
			showModal: false,
			taskToEdit: null,
			filter: {
				title: '',
				description: '',
			},
		};

		this.getTasks = this.getTasks.bind(this);
		this.toggleEditModal = this.toggleEditModal.bind(this);
		this.saveTask = this.saveTask.bind(this);
		this.removeTask = this.removeTask.bind(this);
		this.changeFilterHandle = this.changeFilterHandle.bind(this);
	}

	componentDidMount() {
		this.getTasks();
	}

	async getTasks() {
		const { filter } = this.state;
		const tasks = await helper.getItems(`/tasks${helper.serialize(filter)}`);

		this.setState({ tasks });
	}

	async saveTask(task) {
		if (!task.id) {
			await helper.createItem('/tasks', task);
		} else {
			await helper.patchItem(`/tasks/${task.id}`, task);
		}

		this.setState({ taskToEdit: null, showModal: false }, this.getTasks);
	}

	async removeTask(taskId) {
		const confirmation = confirm('Are you sure to delete it?');

		if (confirmation) {
			await helper.deleteItem(`/tasks/${taskId}`);
			this.getTasks();
		}
	}

	toggleEditModal(task) {
		this.setState({
			showModal: !this.state.showModal,
			taskToEdit: task.id ? task : null,
		});
	}

	changeFilterHandle(event) {
		const val = event.target.value;
		const filter = val.length ? {
			title: val,
			description: val,
		} : {};

		this.setState({ filter }, this.getTasks);
	}

	render() {
		const { tasks, showModal, taskToEdit, filter } = this.state;

		return (
			<div className="tasks-grid">
				<div className="row my-3 px-3 head-row">
					<input type="button" className="btn btn-primary" value="Add" onClick={this.toggleEditModal}/>
					<div className="ml-auto d-flex align-items-center search-area">
						<i className="fa fa-search"></i>
						<Input type="text" className="mx-2" defaultValue={Object.values(filter)[0] || ''} onChange={this.changeFilterHandle}/>
					</div>
				</div>
				<Table hover>
					<thead>
						<tr>
							<th>Day</th>
							<th>Title</th>
							<th>Description</th>
							<th>Updated At</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{tasks.map((task, i) => (
							<Task key={i} task={task} toggleEditModal={this.toggleEditModal} removeTask={this.removeTask}/>
						))}
					</tbody>
				</Table>
				{showModal ?
					<EditTaskModal
						isOpen={showModal}
						toggle={this.toggleEditModal}
						task={taskToEdit}
						saveTask={this.saveTask}
					/> :
					''
				}
			</div>
		);
	}
}

export default TasksGrid;
