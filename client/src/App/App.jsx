import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import './style.scss';

class App extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="container">
				<div className='row w-100 nav navbar-brand'>
					<h2 className='text-center'>TODO LIST</h2>
				</div>
				{this.props.children}
			</div>
		);
	}
}

export default App;
